const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "name is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default:false
		},
		orders: [
			{
				productId: {
					type: Array,
					required: [true, "productId is required"]
				},
				price: {
					type: Number
				},
				totalAmount: {
					type: Number/*,
					required: [true, "total amount is required"]*/
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
);
module.exports = mongoose.model("User", userSchema);