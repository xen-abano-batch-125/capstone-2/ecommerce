const express = require('express');
const router = express.Router();
const auth = require('./../auth');

//controllers
const userController = require("./../controllers/userControllers")

//route that checks if the email exists
router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(result=>res.send(result));
});

//USER REGISTRATION
router.post("/register", (req,res)=> {
	userController.registerUser(req.body).then(result=>res.send(result));
});

//LOGIN USER
router.post("/login", (req,res)=>{
	userController.login(req.body).then(result=>res.send(result));
});

//retrieve details
router.get("/details", auth.verify, (req,res)=> {

	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile(userData.id).then(result=>res.send(result));
})

//set user as admin
router.put('/:userId/setAsAdmin', auth.verify,(req,res)=>{

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	//console.log(data)
	userController.setAsAdmin(data, req.params.userId).then(result => res.send(result));
})

//set user as normal user
router.put('/:userId/setAsNormalUser', auth.verify,(req,res)=>{

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsNormalUser(data, req.params.userId).then(result => res.send(result));
})


router.get('/myOrders', auth.verify,(req,res)=>{
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.myOrders(data).then(result => res.send(result));
})






//get all orders admin only
router.get('/orders', auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(result => res.send(result));
})




//checkout
router.post('/checkout', auth.verify, (req,res)=>{

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	//console.log(data)
	userController.checkout(data, req.body).then(result=>res.send(result));
});

module.exports = router;