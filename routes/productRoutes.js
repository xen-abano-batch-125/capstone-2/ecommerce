const express = require('express');
const router = express.Router();
let auth = require('./../auth')

//controllers
const productController = require('./../controllers/productControllers')

//add product
router.post("/addProduct", auth.verify, (req,res)=> {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data, req.body).then(result=>res.send(result));
});

//retrieve isActive product
router.get("/active", (req,res)=>{

	productController.getAllActiveProducts().then(result => res.send(result));
})

//retrieve all product
router.get("/all", (req,res)=>{

	productController.getAllProducts().then(result => res.send(result));
});

//get single product
router.get("/:productId", auth.verify,(req,res)=>{

	productController.getSingleProduct(req.params).then(result => res.send(result));
})

//update product
router.put('/:productId/edit', auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.editProduct(data, req.params.productId, req.body).then(result => res.send(result));
})

//archive product
router.put('/:productId/archive', auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data, req.params.productId).then(result => res.send(result));
})

//unarchive
router.put('/:productId/unarchive', auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.unarchiveProduct(data, req.params.productId).then(result => res.send(result));
})

//delete product
router.delete('/:productId/delete', auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.deleteProduct(data, req.params.productId).then(result => res.send(result));
})

module.exports = router;