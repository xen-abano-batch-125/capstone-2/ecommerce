const User = require("./../models/User");
const Product = require("./../models/Product");
const Order = require("./../models/Order");
const bcrypt = require('bcrypt');
const auth = require("./../auth");

//add product
module.exports.addProduct = (data, reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		desc: reqBody.desc,
		price: reqBody.price,
		image: reqBody.image
	})

	return newProduct.save().then((result, error)=>{

		if (data.isAdmin === false) {
			return true 
		} else {
			if (error) {
				return error
			} else {
				console.log("product added.")
				return true //if product is added
			}
		}

	});
}

//get active product
module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(result => result)
}

//get all product
module.exports.getAllProducts = ()=> {
	return Product.find({}).then(result => result)
}

//get single product
module.exports.getSingleProduct = (params) => {

	return Product.findById(params.productId).then(product=> product)
}

//update product
module.exports.editProduct = (data, params, reqBody)=> {
	console.log(data)
	let updatedProduct = ({
		name: reqBody.name,
		price: reqBody.price,
		description: reqBody.description,
		image: reqBody.image
	})
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then((result, error) => {
		if (data.isAdmin === false) {
			console.log("NOT AN ADMIN: cannot update product")
			return true
		} else {
			if (error) {
				return error
			} else {
				console.log("PRODUCT UPDATED!")
				return true
			}
		}

	})
}

//archive product
module.exports.archiveProduct = (data, params)=> {

	let updatedActiveProduct = {
		isActive : false
	}

	return Product.findByIdAndUpdate(params, updatedActiveProduct, {new: true}).then((result, error) => {
		if (data.isAdmin === false) {
			//console.log("NOT AN ADMIN: cannot archive product")
			return true
		} else {
			if (error) {
				return false
			} else {
				//console.log("product archived")
				return true
			}
		}
	})
}


//unarchive product
module.exports.unarchiveProduct = (data, params)=> {

	let updatedInactiveProduct = {
		isActive : true
	}

	return Product.findByIdAndUpdate(params, updatedInactiveProduct, {new: true}).then((result, error) => {
		if (data.isAdmin === false) {
			//console.log("NOT AN ADMIN: cannot archive product")
			return true
		} else {
			if (error) {
				return false
			} else {
				//console.log("product archived")
				return true
			}
		}
	})
}


//delete product
module.exports.deleteProduct = (data, params)=> {

	return Product.findByIdAndDelete(params).then((result, error) => {
		if (data.isAdmin === false) {
			return true
		} else {
			if (error) {
				return false
			} else {
				return true
			}
		}
	})
}
